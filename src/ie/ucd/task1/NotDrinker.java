package ie.ucd.task1;

import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;
import ie.ucd.items.AlcoholicDrink;

/*
 * This is a class representing a person who cannot drink alcohol
 */
public class NotDrinker extends Person {

	private String firstName;
	private String lastName;

	// constructor
	public NotDrinker(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/*
	 * Method to allow person to have a drink. Will not allow the person to have the
	 * drink if it is alcoholic.
	 */

	public boolean drink(Drink drink) {
		if (drink instanceof AlcoholicDrink) {
			System.out.print("\nThis person can only drink non-alcoholic drinks");
			return false;
		}
		System.out.print("\nDrink consumed");
		return true;
	}

	// getter method for the persons name
	public String getName() {

		return firstName + " " + lastName;
	}

	public boolean eat(Food arg0) {
		return false;
	}

}
