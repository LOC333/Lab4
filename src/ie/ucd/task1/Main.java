package ie.ucd.task1;

import ie.ucd.items.Wine;
import ie.ucd.items.WineType;

public class Main {

	public static void main(String[] args) {

		Drinker drinker1 = new Drinker("John", "Doe", 80);
		NotDrinker drinker2 = new NotDrinker("Jane", "Doe");
		SoftDrink fanta = new SoftDrink("Fanta", 500); //initialising a non alcoholic drink
		Wine wine = new Wine("Wine", 250, 12.5, WineType.Red); //initialising an alcoholic drink
		drinker1.drink(fanta);
		drinker1.drink(wine);
		drinker2.drink(fanta);
		drinker2.drink(wine); //drinker 2 will not be able to drink the alcoholic drink
		System.out.print("\n" + drinker1.getName() + " is drunk: " + drinker1.isDrunk());
		// have drinker1 drink enough to be drunk
		for (int i = 0; i < 8; i++) {
			drinker1.drink(wine);
		}
		drinker1.getName();
		System.out.print("\n" + drinker1.getName() + " is drunk: " + drinker1.isDrunk());
	}

}
