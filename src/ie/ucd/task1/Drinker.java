package ie.ucd.task1;

import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;
import ie.ucd.items.AlcoholicDrink;

//This class represents a person who drinks alcohol
public class Drinker extends Person {

	// defining parameters associated with the class
	private int numberOfDrinks = 0;
	private double weight;
	private String firstName;
	private String lastName;

	// constructor for Drinker
	public Drinker(String firstName, String lastName, double weight) {
		super();
		this.setWeight(weight);
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/*
	 * Method to allow the person to have a drink. Checks whether the drink is
	 * alcoholic or not and if so it increments the number of drinks the person has
	 * had
	 */
	public boolean drink(Drink drink) {
		if (drink instanceof AlcoholicDrink) {
			numberOfDrinks++;
		}
		return true;
	}

	// getter for the persons name
	public String getName() {
		return firstName + " " + lastName;
	}

	// method to determine if the person is drunk. Criterion is that number of drinks is > weight/10
	public boolean isDrunk() {
		weight = this.getWeight();
		if (numberOfDrinks > (weight / 10)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean eat(Food arg0) {
		return false;
	}

}
