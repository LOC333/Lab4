package ie.ucd.task1;
import ie.ucd.items.NotAlcoholicDrink;
//Class for non alcoholic soft drink
public class SoftDrink extends NotAlcoholicDrink{
	
	//constructor
	public SoftDrink(String drink, double volume) {
		super(drink,volume);
		
	}

}
